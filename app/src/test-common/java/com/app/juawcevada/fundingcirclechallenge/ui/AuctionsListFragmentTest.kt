package com.app.juawcevada.fundingcirclechallenge.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.app.juawcevada.fundingcirclechallenge.R
import com.app.juawcevada.fundingcirclechallenge.shared.Event
import com.app.juawcevada.fundingcirclechallenge.shared.SnackbarMessage
import com.app.juawcevada.fundingcirclechallenge.testing.SingleFragmentActivity
import com.app.juawcevada.fundingcirclechallenge.ui.auctionslist.*
import com.app.juawcevada.fundingcirclechallenge.util.RecyclerViewMatcher
import com.app.juawcevada.fundingcirclechallenge.util.builder.auctionItem
import com.app.juawcevada.fundingcirclechallenge.util.createTestFactory
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuctionsListFragmentTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel:AuctionsListViewModel
    private lateinit var navigationAction: MutableLiveData<Event<AuctionsListNavigationActions>>
    private lateinit var errorMessage: MutableLiveData<Event<SnackbarMessage>>
    private lateinit var viewState: MutableLiveData<AuctionsListViewState>
    private lateinit var fragment: AuctionsListFragmentMockNavigation
    private lateinit var activityScenario: ActivityScenario<SingleFragmentActivity>

    @Before
    fun setUp() {
        activityScenario = ActivityScenario.launch(SingleFragmentActivity::class.java)
        navigationAction = MutableLiveData()
        errorMessage = MutableLiveData()
        viewState = MutableLiveData()
        fragment = AuctionsListFragmentMockNavigation()

        viewModel = mock {
            on {viewState} doReturn this@AuctionsListFragmentTest.viewState
            on {errorMessage} doReturn this@AuctionsListFragmentTest.errorMessage
            on {navigationAction} doReturn this@AuctionsListFragmentTest.navigationAction
        }

        fragment.viewModelFactory = viewModel.createTestFactory()
    }

    @Test
    fun checkViews() {
        val auctionsList = listOf(
            auctionItem {
                id { 1 }
                title { "Toy-Bayer" }
                term { 12 }
            },
            auctionItem {
                id { 2 }
                title { "Emard-Bailey" }
                term { 6 }
            }
            )
        viewState.value = AuctionsListViewState(
            isLoading = false,
            auctionsList = auctionsList
        )
        launchFragment()


        onView(RecyclerViewMatcher(R.id.list).atPosition(0))
            .check(matches(allOf(
                ViewMatchers.hasDescendant(ViewMatchers.withText("Toy-Bayer")),
                ViewMatchers.hasDescendant(ViewMatchers.withText("12 months"))
            )))

        onView(RecyclerViewMatcher(R.id.list).atPosition(1))
            .check(matches(allOf(
                ViewMatchers.hasDescendant(ViewMatchers.withText("Emard-Bailey")),
                ViewMatchers.hasDescendant(ViewMatchers.withText("6 months"))
            )))

    }

    @Test
    fun openDetails() {
        val auctionItem = auctionItem {
            id { 1 }
            title { "Toy-Bayer" }
            term { 12 }
        }
        val auctionsList = listOf(
            auctionItem
        )
        viewState.value = AuctionsListViewState(
            isLoading = false,
            auctionsList = auctionsList
        )
        navigationAction.postValue(Event(AuctionsListNavigationActions.OpenDetail(auctionItem)))
        launchFragment()

        onView(RecyclerViewMatcher(R.id.list).atPosition(0)).perform(click())

        verify(viewModel).openAuction(auctionItem)
        val action = AuctionsListFragmentDirections.actionAuctionsListFragmentToAuctionDetailFragment(auctionItem)
        verify(fragment.navController).navigate(action)
    }

    private fun launchFragment() {
        activityScenario.onActivity {
            it.replaceFragment(fragment)
        }
    }


    class AuctionsListFragmentMockNavigation: AuctionsListFragment() {
        val navController = mock<NavController>()
        override fun navController() = navController
    }

}