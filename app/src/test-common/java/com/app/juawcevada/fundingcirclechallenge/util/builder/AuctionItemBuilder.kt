package com.app.juawcevada.fundingcirclechallenge.util.builder

import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import java.util.*

@AuctionItemDsl
class AuctionItemBuilder {
    private var id: Long = 1
    private var title: String = "Title"
    private var rate: Float = 0.16f
    private var amount: Long = 600000
    private var term: Int = 24
    private var riskBand: String = "A+"
    private var closeTime: Date = Date()
    private var era: Long = 2200

    fun id(body: () -> Long) {
        id = body()
    }

    fun title(body: () -> String) {
        title = body()
    }

    fun rate(body: () -> Float) {
        rate = body()
    }

    fun amount(body: () -> Long) {
        amount = body()
    }

    fun term(body: () -> Int) {
        term = body()
    }

    fun riskBand(body: () -> String) {
        riskBand = body()
    }

    fun closeTime(body: () -> Date) {
        closeTime = body()
    }

    fun era(body: () -> Long) {
        era = body()
    }

    fun build() =
        AuctionItem(
            id,
            title,
            rate,
            amount,
            term,
            riskBand,
            closeTime,
            era
        )
}

fun auctionItem(builder: AuctionItemBuilder.() -> Unit) = AuctionItemBuilder().apply(builder).build()
