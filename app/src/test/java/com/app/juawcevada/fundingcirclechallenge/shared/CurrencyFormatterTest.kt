package com.app.juawcevada.fundingcirclechallenge.shared

import org.junit.Assert
import org.junit.Test

class CurrencyFormatterTest {

    private val currencyFormatter = CurrencyFormatter()

    @Test
    fun formatLongToFloat () {
        val actual = currencyFormatter.toFloat(2000)
        Assert.assertEquals(20.00f, actual, 0.01f)
    }

    @Test
    fun formatFloatToLong () {
        val actual = currencyFormatter.toLong(20.00f)
        Assert.assertEquals(2000, actual)
    }
}