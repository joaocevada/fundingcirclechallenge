package com.app.juawcevada.fundingcirclechallenge.domain.shared

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class EraCalculatorTest(
    private val auctionRate: Float,
    private val riskBand: String,
    private val bidAmount: Long,
    private val expected: Long) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data() : Collection<Array<Any>> {
            return listOf(
                arrayOf(0.16f, "A+", 20000, 22800),
                arrayOf(0.16f, "C-", 20000, 22000),
                arrayOf(0.01f, "C-", 20000, 19000),
                arrayOf(0.20f, "A", 0, 0),
                arrayOf(0.0f, "A+", 20000, 19600)
            )
        }
    }

    private val eraCalculator = EraCalculator(0.01f, BadDebtEstimator())

    @Test
    fun isEraResultValid() {
        val actual = eraCalculator.calculate(auctionRate, riskBand, bidAmount)

        Assert.assertEquals(expected, actual)
    }
}