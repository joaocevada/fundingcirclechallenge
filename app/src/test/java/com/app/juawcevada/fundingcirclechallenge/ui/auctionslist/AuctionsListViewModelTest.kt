package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.core.Try
import com.app.juawcevada.fundingcirclechallenge.R
import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import com.app.juawcevada.fundingcirclechallenge.domain.auctions.GetAuctionsUseCase
import com.app.juawcevada.fundingcirclechallenge.shared.AppDispatchers
import com.app.juawcevada.fundingcirclechallenge.shared.CurrencyFormatter
import com.app.juawcevada.fundingcirclechallenge.util.builder.auctionItem
import com.app.juawcevada.fundingcirclechallenge.util.observeTest
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class AuctionsListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val appTestDispatchers =
        AppDispatchers(
            Dispatchers.Unconfined,
            Dispatchers.Unconfined,
            Dispatchers.Unconfined
        )

    private val currencyFormatter = CurrencyFormatter()

    private val defaultBid: Long = 2000

    @Test
    fun loadEmptyList() = runBlocking {
        val getAuctionsUseCase: GetAuctionsUseCase = mock {
            onBlocking { invoke(any()) } doReturn Try.just(emptyList())
        }

        val viewModel =
            AuctionsListViewModel(
                getAuctionsUseCase,
                appTestDispatchers,
                currencyFormatter,
                defaultBid
            ).apply {
                viewState.observeTest()
                errorMessage.observeTest()
                navigationAction.observeTest()
            }

        with(viewModel.viewState.value!!) {
            Assert.assertEquals(emptyList<AuctionItem>(), auctionsList)
            Assert.assertEquals(false, isLoading)
            Assert.assertNull(errorMessage)
        }
        with(viewModel.errorMessage.value) {
            Assert.assertNull(this)
        }
        with(viewModel.navigationAction.value) {
            Assert.assertNull(this)
        }
    }

    @Test
    fun loadError() = runBlocking {
        val getAuctionsUseCase: GetAuctionsUseCase = mock {
            onBlocking { invoke(any()) } doReturn Try.raise(Exception())
        }

        val viewModel =
            AuctionsListViewModel(
                getAuctionsUseCase,
                appTestDispatchers,
                currencyFormatter,
                defaultBid
            ).apply {
                viewState.observeTest()
                errorMessage.observeTest()
                navigationAction.observeTest()
            }

        with(viewModel.viewState.value!!) {
            Assert.assertNull(auctionsList)
            Assert.assertEquals(false, isLoading)
            Assert.assertEquals(Exception().toString(), errorMessage)
        }
        with(viewModel.errorMessage.value!!) {
            Assert.assertEquals(R.string.oops, getContentIfNotHandled()!!.messageId)
        }
        with(viewModel.navigationAction.value) {
            Assert.assertNull(this)
        }
    }

    @Test
    fun loadTwoElementList() = runBlocking {
        val expectedList = listOf(
            auctionItem {
                id {1}
            },
            auctionItem {
                id {2}
            }
        )

        val getAuctionsUseCase: GetAuctionsUseCase = mock {
            onBlocking { invoke(any()) } doReturn Try.just(expectedList)
        }

        val viewModel =
            AuctionsListViewModel(
                getAuctionsUseCase,
                appTestDispatchers,
                currencyFormatter,
                defaultBid
            ).apply {
                viewState.observeTest()
                errorMessage.observeTest()
                navigationAction.observeTest()
            }

        with(viewModel.viewState.value!!) {
            Assert.assertEquals(expectedList, auctionsList)
            Assert.assertEquals(false, isLoading)
            Assert.assertNull(errorMessage)
        }
        with(viewModel.errorMessage.value) {
            Assert.assertNull(this)
        }
        with(viewModel.navigationAction.value) {
            Assert.assertNull(this)
        }
    }

    @Test
    fun loadBidUpdateList() = runBlocking {
        val getAuctionsUseCase: GetAuctionsUseCase = mock {
            onBlocking { invoke(any()) } doReturn Try.just(emptyList())
        }

        val viewModel =
            AuctionsListViewModel(
                getAuctionsUseCase,
                appTestDispatchers,
                currencyFormatter,
                defaultBid
            ).apply {
                viewState.observeTest()
                errorMessage.observeTest()
                navigationAction.observeTest()
            }

        viewModel.changeBid(50.0f)

        with(viewModel.viewState.value!!) {
            Assert.assertEquals(emptyList<AuctionItem>(), auctionsList)
            Assert.assertEquals(false, isLoading)
            Assert.assertEquals(5000, bid)
            Assert.assertNull(errorMessage)
        }
        with(viewModel.errorMessage.value) {
            Assert.assertNull(this)
        }
        with(viewModel.navigationAction.value) {
            Assert.assertNull(this)
        }
    }

    @Test
    fun loadUpdateLoading() = runBlocking {
        val getAuctionsUseCase: GetAuctionsUseCase = mock {
            onBlocking { invoke(any()) } doReturn Try.just(emptyList())
        }

        val viewModel =
            AuctionsListViewModel(
                getAuctionsUseCase,
                AppDispatchers(
                    Dispatchers.IO,
                    Dispatchers.IO,
                    Dispatchers.IO
                ),
                currencyFormatter,
                defaultBid
            ).apply {
                viewState.observeTest()
                errorMessage.observeTest()
                navigationAction.observeTest()
            }

        with(viewModel.viewState.value!!) {
            Assert.assertNull(auctionsList)
            Assert.assertEquals(true, isLoading)
            Assert.assertNull(errorMessage)
        }
        with(viewModel.errorMessage.value) {
            Assert.assertNull(this)
        }
        with(viewModel.navigationAction.value) {
            Assert.assertNull(this)
        }
    }
}