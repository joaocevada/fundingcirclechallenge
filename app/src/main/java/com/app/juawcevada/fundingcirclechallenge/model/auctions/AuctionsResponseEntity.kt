package com.app.juawcevada.fundingcirclechallenge.model.auctions

data class AuctionsResponseEntity(val items: List<AuctionEntity>)