package com.app.juawcevada.fundingcirclechallenge.di

import com.app.juawcevada.fundingcirclechallenge.ui.MainActivity
import com.app.juawcevada.fundingcirclechallenge.ui.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun bindMainActivity(): MainActivity
}