package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem

sealed class AuctionsListNavigationActions {
    class OpenDetail(val item: AuctionItem) : AuctionsListNavigationActions()
}