package com.app.juawcevada.fundingcirclechallenge.domain.auctions

import com.app.juawcevada.fundingcirclechallenge.domain.shared.EraCalculator
import com.app.juawcevada.fundingcirclechallenge.model.auctions.AuctionEntity
import dagger.Reusable
import javax.inject.Inject

@Reusable
class AuctionEntityToAuctionItemMapper @Inject constructor(private val eraCalculator: EraCalculator){

    fun mapFrom(from: AuctionEntity, bidAmount: Long): AuctionItem {
        return with(from) {
            AuctionItem(
                id,
                title,
                rate,
                amountCents,
                term,
                riskBand,
                closeTime,
                eraCalculator.calculate(rate, riskBand, bidAmount)
            )
        }
    }
}