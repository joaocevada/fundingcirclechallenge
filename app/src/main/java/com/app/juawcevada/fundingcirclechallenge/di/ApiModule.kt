package com.app.juawcevada.fundingcirclechallenge.di

import com.app.juawcevada.fundingcirclechallenge.data.ApiService
import com.app.juawcevada.fundingcirclechallenge.di.annotation.ApiConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import dagger.Reusable
import java.util.*


/**
 * App module
 */
@Module
class ApiModule {

    @Provides
    @Reusable
    internal fun provideApi(
        @ApiConfig url: String,
        okHttpClient: OkHttpClient): ApiService {

        val moshi = Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .build()

        val retrofit = Retrofit.Builder()
                .callFactory(okHttpClient)
                .baseUrl(url)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()

        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Reusable
    internal fun provideOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BASIC
        }

        return OkHttpClient.Builder().addInterceptor(logging).build()
    }
}