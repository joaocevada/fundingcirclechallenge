package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import com.app.juawcevada.fundingcirclechallenge.ui.shared.LceViewActions

interface AuctionsListViewActions : LceViewActions {
    fun openAuction(item: AuctionItem)
    fun changeBid(bid: Float)
}