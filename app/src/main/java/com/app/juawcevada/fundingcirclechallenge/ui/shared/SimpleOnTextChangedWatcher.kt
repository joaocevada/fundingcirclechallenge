package com.app.juawcevada.fundingcirclechallenge.ui.shared

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class SimpleOnTextChangedWatcher(val onTextChanged: (String) -> Unit) : TextWatcher {

    override fun afterTextChanged(s: Editable?) {
        onTextChanged(s.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }
}


fun EditText.onTextChanged(onTextChanged: (String) -> Unit) {
    this.addTextChangedListener(SimpleOnTextChangedWatcher(onTextChanged))
}