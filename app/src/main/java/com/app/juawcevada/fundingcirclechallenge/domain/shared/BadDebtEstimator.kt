package com.app.juawcevada.fundingcirclechallenge.domain.shared

import dagger.Reusable
import java.lang.Exception
import javax.inject.Inject

@Reusable
class BadDebtEstimator @Inject constructor() {
    fun estimate(riskBand: String): Float = when (riskBand) {
        "A+" -> 0.01f
        "A" -> 0.02f
        "B" -> 0.03f
        "C" -> 0.04f
        "C-" -> 0.05f
        else -> throw Exception ("Invalid riskBand")
    }
}