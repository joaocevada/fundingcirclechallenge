package com.app.juawcevada.fundingcirclechallenge.data.auctions

import arrow.core.Try
import com.app.juawcevada.fundingcirclechallenge.data.ApiService
import com.app.juawcevada.fundingcirclechallenge.model.auctions.AuctionEntity
import dagger.Reusable
import ru.gildor.coroutines.retrofit.await
import javax.inject.Inject

@Reusable
class AuctionsRepository @Inject constructor(
    private val auctionsService: ApiService
) {

    suspend fun getAuctions(): Try<List<AuctionEntity>> = Try {
        auctionsService.getAuctions().await().items
    }
}