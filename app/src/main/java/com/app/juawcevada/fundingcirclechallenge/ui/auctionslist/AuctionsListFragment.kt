package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.app.juawcevada.fundingcirclechallenge.databinding.AuctionsListFragmentBinding
import com.app.juawcevada.fundingcirclechallenge.shared.EventObserver
import com.app.juawcevada.fundingcirclechallenge.shared.extensions.setUpSnackbar
import com.app.juawcevada.fundingcirclechallenge.shared.extensions.viewModelProvider
import com.app.juawcevada.fundingcirclechallenge.testing.OpenClassOnDebug
import com.app.juawcevada.fundingcirclechallenge.ui.shared.onTextChanged
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

@OpenClassOnDebug
class AuctionsListFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AuctionsListViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(viewModelFactory)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val binding: AuctionsListFragmentBinding =
            AuctionsListFragmentBinding.inflate(inflater, container, false).also {
                it.setLifecycleOwner(this)
                it.viewModel = viewModel
                it.viewActions = viewModel
                it.list.apply {
                    setHasFixedSize(true)
                    adapter = AuctionsListAdapter(
                        viewModel::openAuction
                    )
                }
                it.bidText.onTextChanged { bid ->
                    viewModel.changeBid(bid.toFloatOrNull() ?: 0.0f)
                }

            }

        viewModel.navigationAction.observe(viewLifecycleOwner, EventObserver { event ->

            when (event) {
                is AuctionsListNavigationActions.OpenDetail -> {
                   val action
                           = AuctionsListFragmentDirections
                       .actionAuctionsListFragmentToAuctionDetailFragment(event.item)
                    navController().navigate(action)
                }
            }
        })

        setUpSnackbar(viewModel.errorMessage, binding.root)

        return binding.root
    }

    /**
     * Created to be able to override in tests
     */
    @VisibleForTesting
    fun navController() = findNavController()

}