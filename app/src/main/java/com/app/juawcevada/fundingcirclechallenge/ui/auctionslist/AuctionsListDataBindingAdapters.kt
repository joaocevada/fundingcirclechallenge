package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.juawcevada.fundingcirclechallenge.R
import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import com.app.juawcevada.fundingcirclechallenge.shared.CurrencyFormatter
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.auction_item.view.*


@BindingAdapter("setAuctionsList")
fun setAuctionsList(recyclerView: RecyclerView, list: List<AuctionItem>?) {
    (recyclerView.adapter as AuctionsListAdapter).submitList(list)
}

@BindingAdapter("setBid")
fun setBid(editText: TextInputEditText, bid: Long) {
    val currencyFormatter = CurrencyFormatter() // In the future use a DataBindingComponent to get this dependency
    editText.setText(currencyFormatter.toFloat(bid).toString(), TextView.BufferType.EDITABLE)
}

@BindingAdapter("setReturnAmount")
fun setReturnAmount(textView: TextView, era: Long) {
    val currencyFormatter = CurrencyFormatter() // In the future use a DataBindingComponent to get this dependency
    val twoDigitRoundedEra = String.format("%.2f", currencyFormatter.toFloat(era))
    textView.text = textView.context.getString(R.string.estimated_return_amount, twoDigitRoundedEra)
}