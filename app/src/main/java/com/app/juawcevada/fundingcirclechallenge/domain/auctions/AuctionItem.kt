package com.app.juawcevada.fundingcirclechallenge.domain.auctions

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class AuctionItem (
    val id: Long,
    val title: String,
    val rate: Float,
    val amount: Long,
    val term: Int,
    val riskBand: String,
    val closeTime: Date,
    val era: Long
) : Parcelable