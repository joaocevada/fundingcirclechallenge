package com.app.juawcevada.fundingcirclechallenge.ui.shared


import androidx.lifecycle.MediatorLiveData
import com.app.juawcevada.fundingcirclechallenge.shared.AppDispatchers
import kotlinx.coroutines.withContext

/**
 * MediatorLiveData with helper functions to help update a ViewState
 */
class ViewStateLiveData<T>(initialState: T) : MediatorLiveData<T>() {

    init {
        value = initialState
    }

    val currentState: T
        get() = value!!

    fun updateState(newStateCreator: T.() -> T) {
        value = value!!.newStateCreator()
    }

}