package com.app.juawcevada.fundingcirclechallenge.di.annotation

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiConfig