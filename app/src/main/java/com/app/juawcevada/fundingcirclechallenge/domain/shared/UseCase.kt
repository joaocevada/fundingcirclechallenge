package com.app.juawcevada.fundingcirclechallenge.domain.shared

import arrow.core.Try


abstract class UseCase<in P, R> {

    suspend operator fun invoke(parameters: P): Try<R> = execute(parameters)

    protected abstract suspend fun execute(parameters: P): Try<R>
}
