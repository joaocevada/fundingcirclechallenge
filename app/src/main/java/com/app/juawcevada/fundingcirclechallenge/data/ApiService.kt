package com.app.juawcevada.fundingcirclechallenge.data


import com.app.juawcevada.fundingcirclechallenge.model.auctions.AuctionsResponseEntity
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("auctions")
    fun getAuctions(): Call<AuctionsResponseEntity>
}