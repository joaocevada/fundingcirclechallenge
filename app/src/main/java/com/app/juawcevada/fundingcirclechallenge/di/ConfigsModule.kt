package com.app.juawcevada.fundingcirclechallenge.di

import com.app.juawcevada.fundingcirclechallenge.di.annotation.ApiConfig
import com.app.juawcevada.fundingcirclechallenge.di.annotation.BidDefault
import com.app.juawcevada.fundingcirclechallenge.di.annotation.FeeDefault
import dagger.Module
import dagger.Provides


@Module
class ConfigsModule {

    @Provides
    @ApiConfig
    fun providesApiUrl(): String {
        return "http://fc-ios-test.herokuapp.com/"
    }

    @Provides
    @BidDefault
    fun providesEra(): Long {
        return 2000
    }

    @Provides
    @FeeDefault
    fun providesFee(): Float {
        return 0.001f
    }
}