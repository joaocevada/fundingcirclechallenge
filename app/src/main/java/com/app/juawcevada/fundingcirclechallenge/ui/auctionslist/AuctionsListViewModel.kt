package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.app.juawcevada.fundingcirclechallenge.R
import com.app.juawcevada.fundingcirclechallenge.di.annotation.BidDefault
import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import com.app.juawcevada.fundingcirclechallenge.domain.auctions.GetAuctionsUseCase
import com.app.juawcevada.fundingcirclechallenge.shared.AppDispatchers
import com.app.juawcevada.fundingcirclechallenge.shared.CurrencyFormatter
import com.app.juawcevada.fundingcirclechallenge.shared.Event
import com.app.juawcevada.fundingcirclechallenge.shared.SnackbarMessage
import com.app.juawcevada.fundingcirclechallenge.testing.OpenClassOnDebug
import com.app.juawcevada.fundingcirclechallenge.ui.shared.ScopedViewModel
import com.app.juawcevada.fundingcirclechallenge.ui.shared.ViewStateLiveData
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@OpenClassOnDebug
class AuctionsListViewModel @Inject constructor(
    private val getAuctionsUseCase: GetAuctionsUseCase,
    private val appDispatchers: AppDispatchers,
    private val currencyFormatter: CurrencyFormatter,
    @BidDefault val defaultBid: Long
) : ScopedViewModel(), AuctionsListViewActions {

    private val _viewState = ViewStateLiveData(AuctionsListViewState())
    val viewState: LiveData<AuctionsListViewState>
        get() = _viewState

    private val _errorMessage = MutableLiveData<Event<SnackbarMessage>>()
    val errorMessage: LiveData<Event<SnackbarMessage>>
        get() = _errorMessage

    private val _navigationAction = MutableLiveData<Event<AuctionsListNavigationActions>>()
    val navigationAction: LiveData<Event<AuctionsListNavigationActions>>
        get() = _navigationAction

    private val viewStateActor = actor<AuctionEvent>(
        context = appDispatchers.Main,
        capacity = Channel.CONFLATED
    ) {
        with(_viewState) {
            for (msg in channel) {
                when (msg) {
                    is AuctionEvent.LoadBid -> {
                        updateState {
                            copy(
                                bid = msg.bid,
                                isLoading = true,
                                auctionsList = emptyList()
                            )
                        }

                        withContext(appDispatchers.IO) {
                            getAuctionsUseCase(msg.bid)
                        }.fold(
                            { throwable ->
                                updateState {
                                    copy(isLoading = false,
                                        auctionsList = null,
                                        errorMessage = throwable.toString()
                                    )
                                }
                                _errorMessage.value = Event(SnackbarMessage(R.string.oops))
                            },
                            { list ->
                                updateState {
                                    copy(
                                        isLoading = false,
                                        auctionsList = list,
                                        errorMessage = null
                                    )
                                }
                            })
                    }
                }
            }
        }
    }

    init {
        launch(appDispatchers.Default) {
            viewStateActor.send(AuctionEvent.LoadBid(defaultBid))
        }
    }

    override fun retry() {
        launch(appDispatchers.Default) {
            viewStateActor.send(AuctionEvent.LoadBid(_viewState.currentState.bid))
        }
    }

    override fun openAuction(item: AuctionItem) {
        _navigationAction.postValue(Event(AuctionsListNavigationActions.OpenDetail(item)))
    }

    override fun changeBid(bid: Float) {
        val newBid = currencyFormatter.toLong(bid)
        if (_viewState.currentState.bid == newBid) return

        launch(appDispatchers.Default) {
            viewStateActor.send(AuctionEvent.LoadBid(newBid))
        }
    }

    sealed class AuctionEvent {
        data class LoadBid(val bid: Long) : AuctionEvent()
    }
}