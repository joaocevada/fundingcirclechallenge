package com.app.juawcevada.fundingcirclechallenge.model.auctions

import com.squareup.moshi.Json
import java.util.*

data class AuctionEntity(
    val id: Long,
    val title: String,
    val rate: Float,
    @field:Json(name = "amount_cents") val amountCents: Long,
    val term: Int,
    @field:Json(name = "risk_band") val riskBand: String,
    @field:Json(name = "close_time") val closeTime: Date)