package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import com.app.juawcevada.fundingcirclechallenge.ui.shared.LceViewState


data class AuctionsListViewState(
    override val isLoading: Boolean = true,
    val auctionsList: List<AuctionItem>? = null,
    val bid: Long = 0,
    override val errorMessage: String? = null
) : LceViewState