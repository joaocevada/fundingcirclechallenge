package com.app.juawcevada.fundingcirclechallenge.di

import android.content.Context
import com.app.juawcevada.fundingcirclechallenge.Application
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    internal abstract fun provideContext(application: Application): Context
}