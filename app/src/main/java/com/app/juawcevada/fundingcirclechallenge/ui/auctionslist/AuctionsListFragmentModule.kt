package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class AuctionsListFragmentModule {

    @Binds
    internal abstract fun bindFragment(fragment: AuctionsListFragment): Fragment
}