package com.app.juawcevada.fundingcirclechallenge.ui.auctionslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.app.juawcevada.fundingcirclechallenge.R
import com.app.juawcevada.fundingcirclechallenge.databinding.AuctionItemBinding
import com.app.juawcevada.fundingcirclechallenge.domain.auctions.AuctionItem
import com.app.juawcevada.fundingcirclechallenge.ui.shared.ListRecyclerAdapter

class AuctionsListAdapter(
    private val onAuctionTap: (AuctionItem) -> Unit
) : ListRecyclerAdapter<AuctionItem, AuctionItemBinding>(ItemDiffCallback) {

    override fun bind(binding: AuctionItemBinding, item: AuctionItem, position: Int, payloads: MutableList<Any>) {
        binding.item = item
        binding.root.tag = item
    }

    override fun createBinding(parent: ViewGroup): AuctionItemBinding {
        val layoutInflater = LayoutInflater.from(parent.context)
        return DataBindingUtil.inflate<AuctionItemBinding>(
            layoutInflater,
            R.layout.auction_item,
            parent,
            false
        ).apply {
            root.setOnClickListener { view ->
                (view.tag as? AuctionItem)?.let { item ->
                    onAuctionTap(item)
                }
            }
        }
    }


    object ItemDiffCallback : DiffUtil.ItemCallback<AuctionItem>() {
        override fun areItemsTheSame(oldItem: AuctionItem, newItem: AuctionItem) =
            newItem.id == oldItem.id


        override fun areContentsTheSame(oldItem: AuctionItem, newItem: AuctionItem) = newItem == oldItem
    }
}