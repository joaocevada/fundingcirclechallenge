package com.app.juawcevada.fundingcirclechallenge.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.juawcevada.fundingcirclechallenge.di.annotation.ViewModelKey
import com.app.juawcevada.fundingcirclechallenge.ui.auctionslist.AuctionsListViewModel
import com.app.juawcevada.fundingcirclechallenge.ui.shared.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Provides the view model factory with view models maps and binds the ViewModelFactory type to
 * a ViewModelProvider.Factory type.
 */
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AuctionsListViewModel::class)
    internal abstract fun bindCharacterDetailsViewModel(viewModel: AuctionsListViewModel): ViewModel


    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}