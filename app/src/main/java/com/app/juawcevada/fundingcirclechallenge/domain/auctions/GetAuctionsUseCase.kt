package com.app.juawcevada.fundingcirclechallenge.domain.auctions

import arrow.core.Try
import com.app.juawcevada.fundingcirclechallenge.data.auctions.AuctionsRepository
import com.app.juawcevada.fundingcirclechallenge.domain.shared.UseCase
import dagger.Reusable
import javax.inject.Inject

@Reusable
class GetAuctionsUseCase @Inject constructor(
    private val auctionsRepository: AuctionsRepository,
    private val auctionEntityToItemMapper: AuctionEntityToAuctionItemMapper
) : UseCase<Long, List<AuctionItem>>() {

    override suspend fun execute(parameters: Long): Try<List<AuctionItem>> {
        return auctionsRepository.getAuctions().map { list -> list.map {
            auctionEntityToItemMapper.mapFrom(it, parameters) }
        }
    }
}