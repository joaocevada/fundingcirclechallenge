package com.app.juawcevada.fundingcirclechallenge.domain.shared

import com.app.juawcevada.fundingcirclechallenge.di.annotation.FeeDefault
import dagger.Reusable
import javax.inject.Inject
import kotlin.math.roundToLong

@Reusable
class EraCalculator @Inject constructor(
    @FeeDefault private val fee: Float, private val badDebtEstimator: BadDebtEstimator
) {

    fun calculate(auctionRate: Float, riskBand: String, bidAmount: Long): Long {
        val badDebt = badDebtEstimator.estimate(riskBand)

        return ((1.0 + auctionRate - badDebt - fee) * bidAmount).roundToLong()
    }
}