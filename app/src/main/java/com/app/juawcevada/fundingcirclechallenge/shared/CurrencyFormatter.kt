package com.app.juawcevada.fundingcirclechallenge.shared

import dagger.Reusable
import javax.inject.Inject
import kotlin.math.roundToLong

@Reusable
class CurrencyFormatter @Inject constructor(){

    fun toFloat(amount: Long) : Float = amount / 100.0f
    fun toLong(amount: Float) : Long = (amount * 100.0f).roundToLong()
}