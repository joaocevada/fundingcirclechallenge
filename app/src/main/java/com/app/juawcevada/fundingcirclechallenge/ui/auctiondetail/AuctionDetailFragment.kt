package com.app.juawcevada.fundingcirclechallenge.ui.auctiondetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.juawcevada.fundingcirclechallenge.databinding.AuctionsDetailFragmentBinding
import dagger.android.support.AndroidSupportInjection

class AuctionDetailFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val item = AuctionDetailFragmentArgs.fromBundle(arguments).item

        val binding =
            AuctionsDetailFragmentBinding
                .inflate(
                    inflater,
                    container,
                    false).also {
                    it.item = item
                }

        return binding.root
    }
}