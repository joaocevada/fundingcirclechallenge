package com.app.juawcevada.fundingcirclechallenge.ui

import com.app.juawcevada.fundingcirclechallenge.ui.auctionslist.AuctionsListFragment
import com.app.juawcevada.fundingcirclechallenge.ui.auctionslist.AuctionsListFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [AuctionsListFragmentModule::class])
    internal abstract fun contributeAuctionsListFragment(): AuctionsListFragment

}