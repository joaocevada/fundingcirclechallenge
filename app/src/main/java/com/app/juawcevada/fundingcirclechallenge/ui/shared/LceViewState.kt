package com.app.juawcevada.fundingcirclechallenge.ui.shared

interface LceViewState {
    val isLoading: Boolean
    val errorMessage: String?
}