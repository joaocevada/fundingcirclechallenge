package com.app.juawcevada.fundingcirclechallenge.testing

@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class OpenClass