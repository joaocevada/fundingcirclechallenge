package com.app.juawcevada.fundingcirclechallenge.testing

@OpenClass
@Target(AnnotationTarget.CLASS)
annotation class OpenClassOnDebug